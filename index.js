#!/usr/bin/env node
const fs = require('fs');

const validateChanges=(modifiedPaths)=>{
    const ranComplete = modifiedPaths.every(path=>{
        if(path.includes('cspell.json')){
            const cspellData = fs.readFileSync(path, 'utf8');
            if(cspellData){
                const cspellJson = JSON.parse(cspellData);
                if(cspellJson && (cspellJson.hasOwnProperty('words') || cspellJson.hasOwnProperty('ignoreWords'))){
                    console.error("Error with cspell.json configuration:\nDon't extend words into configuration directly to stop spell check, whitelist them via cspell ignore comments within file. \nFollow this more information https://cspell.org/configuration/document-settings/#enable--disable-checking-sections-of-code");
                    process.exitCode = 1;
                    return false;
                }
            }
        }
        return true;
    })
    if(!ranComplete) return;
    process.exitCode=0;
}
const [,,...modifiedPaths] = process.argv;
if (!modifiedPaths.length) {
    console.error("Usage: cspell-config <regexp>");
    process.exitCode = 1;
    return;
}

if(modifiedPaths.includes('stdin')) {
    const stdinData = [];
    process.stdin.setEncoding('ascii'); 
    process.stdin.on('data', function (data) {
        stdinData.push(...data.split('\n').filter(value=>value));
    }); 
 
    process.stdin.on('end', function () { 
        validateChanges(stdinData);
    }); 
}
else {
    validateChanges(modifiedPaths);
}
